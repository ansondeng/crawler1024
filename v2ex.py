#coding=utf-8
#__author__ = 'thinkpad'

#!usr/bin/python

import os
import re
import cl_1024
from HTMLParser import HTMLParser


class v2ex_parser(HTMLParser):
    def __init__(self, min_count):
        HTMLParser.__init__(self)
        self.min_count = min_count
        self.pages = []
        self.tmp_url = ""
        self.is_count_td = False
        self.is_a = False
        self.tmp_title = ""
    def handle_starttag(self, tag, attrs):
        #print "Encountered an start tag :", tag
        if tag == "td":
            for attr in attrs:
                if attr[0] == 'width' and attr[1] == '50':
                    self.is_count_td = True
                    break
        elif self.is_count_td == True and tag == 'a':
            for attr in attrs:
                if attr[0] == 'href':
                    self.is_a = True
                    self.tmp_url = attr[1]
                    #print "url ", self.tmp_url
                    break
    def handle_endtag(self, tag):
        #print "Encountered an end tag :", tag
        if tag == "td":
            self.is_count_td = False
            self.is_a = False
        elif tag == 'a':
            self.is_a = False;
    def handle_data(self, data):
        #print "Encountered some data  :", data
        if(self.is_a):
            #print "count data ", data
            backcount = int(data)
            self.is_checked = True
            if(backcount > self.min_count):
                print "  find ", backcount
                tmp = {"title":self.tmp_title, "url":self.tmp_url};
                self.pages.append(tmp);
    def get_result(self):
        return self.pages


def save_page(url, sub_dir, name):
    page = cl_1024.fetch_content(url)
    page = re.sub(r'(src|href)=\"/(t|css|static)/(.*)\"',  r'\1="http://v2ex.com/\2/\3"', page)
    page = re.sub(r'(img|script) src=\"//(cdn)(.*)\"',  r'img src="http://\2\3"', page)
    dir = os.getcwd()
    if not os.path.exists(dir+"\\"+sub_dir) :
        os.mkdir(sub_dir);
    f = file(dir + "\\"+sub_dir+"\\"+name+".html", 'w')
    f.write(page)
    f.close()



def main():
    v2ex_base = 'http://v2ex.com'
    url_base = 'http://v2ex.com/recent?p='
    min_count = 100
    index_count = 100
    save_count = 1

    index = 1
    while index < index_count:
        url = url_base + str(index)
        index += 1
        print "1 获取页面 ", url
        content = cl_1024.fetch_content(url)
        if content == "":
            print "no more page in ", url
            break
        else:
            print "  2 搜索页面 ", url
            my_parser = v2ex_parser(min_count)
            pages = cl_1024.process(my_parser, content)
            for page in pages:
                print "    3 保存网页 ", page['url']
                save_page(v2ex_base+page['url'], "v2ex", str(save_count))
                save_count += 1
    print "save ", save_count-1, " pages"

if __name__ == '__main__':
    main()