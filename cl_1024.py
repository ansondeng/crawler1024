#coding=utf-8
#__author__ = 'thinkpad'

#!usr/bin/python

import os
import re
import urllib2
import httplib
import sys
from HTMLParser import HTMLParser


class cl_parser(HTMLParser):
    def __init__(self, min_count):
        HTMLParser.__init__(self)
        self.min_count = min_count
        self.pages = []
        self.tmp_url = ""
        self.is_tr = False
        self.is_head_td = False
        self.is_h3 = False
        self.tmp_title = ""
        self.is_count_td = False
        self.is_checked = False
    def handle_starttag(self, tag, attrs):
        #print "Encountered a start tag:", tag
        if(tag == "tr"):
            self.is_tr = True
        elif(True == self.is_tr and tag == "td"):
            for attr in attrs:
                if(attr[0] == "style" and attr[1] == "text-align:left;padding-left:8px"):
                    self.is_head_td = True
                elif(attr[0] == "class" and attr[1] == "tal f10 y-style"):
                    self.is_count_td = True
        elif(True == self.is_head_td and tag == "h3"):
            self.is_h3 = True
        elif(True == self.is_h3 and tag == "a"):
            for attr in attrs:
                if(attr[0] == "href"):
                    self.tmp_url = attr[1]
    def handle_endtag(self, tag):
        #print "Encountered an end tag :", tag
        if(tag == "tr"):
            self.is_tr = False
            self.is_head_td = False
            self.is_h3 = False
            self.is_count_td = False
            self.is_checked = False
            self.tmp_url = ""
            self.tmp_title = "title"
        elif(tag == "h3"):
            self.is_h3 = False
    def handle_data(self, data):
        #print "Encountered some data  :", data
        if(self.is_count_td and (not self.is_checked)):
            #print "count data ", data
            backcount = int(data)
            self.is_checked = True
            if(backcount > self.min_count):
                print "  find ", backcount
                tmp = {"title":self.tmp_title, "url":self.tmp_url};
                self.pages.append(tmp);
        elif(self.is_h3):
            self.tmp_title = data.decode('gbk').encode('utf-8');
    def get_result(self):
        return self.pages


def process(parser, content):
    try:
        content = re.sub("<a href=profile.*>", '<a>', content);
        parser.feed(content)
        return parser.get_result()
    except Exception, e:
        print "  解析失败 ", e
        ret = [];
        return ret;


def fetch_content(url):
    try_times = 3
    while try_times > 0:
        try:
            content = urllib2.urlopen(url).read()
        except httplib.IncompleteRead, e:
            print "fetch_content error1"
            print e.partial
            print sys.exc_info()
            print e
            f = file("IncompleteRead.html", 'w');
            f.write(e.partial)
            f.close()
            content = ""
        except Exception, e:
            print "fetch_content error2"
            print sys.exc_info()
            print e
            content = ""
        if content == "":
            print "~~~~~ try again"
            try_times -= 1
        else:
            break;
    return content


def save_page(url, sub_dir, name):
    page = fetch_content(url)
    dir = os.getcwd()
    if not os.path.exists(dir+"\\"+sub_dir) :
        os.mkdir(sub_dir);
    f = file(dir + "\\"+sub_dir+"\\"+name+".html", 'w')
    f.write(page)
    f.close()


def test_parser():
    f = file("cl_parser_test.html", 'r')
    content = f.read()
    #content = fetch_content("http://5.yao.cl/thread0806.php?fid=2")
    urls = process(content)
    for url in urls:
        print url


def main():
    test_url = "http://www.baidu.com"
    cl_base = "http://5.yao.cl/"
    cl_1 = "http://5.yao.cl/thread0806.php?fid=2&search=&page="  #无码
    cl_2 = "http://5.yao.cl/thread0806.php?fid=15&search=&page=" #有码
    cl_3 = "http://5.yao.cl/thread0806.php?fid=7&search=&page=" #讨论
    cl_4 = "http://5.yao.cl/thread0806.php?fid=8&search=&page=" #图片
    cl_5 = "http://5.yao.cl/thread0806.php?fid=16&search=&page=" #自拍
    url_base_list = [cl_3]
    min_count = 1000
    index_count = 269
    save_count = 1
    for url_base in url_base_list:
        index = 1
        while index < index_count:
            url = url_base + str(index)
            index += 1
            print "1 获取页面 ", url
            content = fetch_content(url)
            #content = fetch_content(test_url)
            if content == "":
                print "no more page in ", url
                break
            else:
                print "  2 搜索页面 ", url
                my_parser = cl_parser(min_count)
                pages = process(my_parser, content)
                for page in pages:
                    print "    3 保存网页 ", page['title']
                    save_page(cl_base+page['url'], "cl", str(save_count))
                    save_count += 1
    print "save ", save_count-1, " pages"



if __name__ == '__main__':
    main()
    #test_parser()

